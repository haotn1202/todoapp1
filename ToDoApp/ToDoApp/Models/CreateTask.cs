﻿namespace ToDoApp.Models
{
    public class CreateTaskView
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? deadline { get; set; }
        public int priority { get; set; }
        public int status { get; set; }

        public CreateTaskView() { }

        public CreateTaskView(string name, string description, DateTime? deadline, int priority, int status)
        {
            Name = name;
            Description = description;
            this.deadline = deadline;
            this.priority = priority;
            this.status = status;
        }
    }


}
