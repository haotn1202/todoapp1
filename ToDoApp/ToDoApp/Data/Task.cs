﻿namespace ToDoApp.Data
{
    public class Task
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? deadline { get; set; }
        public int priority { get; set; }
        public int status { get; set; }


        public Task() { }

        public Task(string id, string name, string description, DateTime? deadline, int priority, int status)
        {
            Id = id;
            Name = name;
            Description = description;
            this.deadline = deadline;
            this.priority = priority;
            this.status = status;
        }
    }
}
