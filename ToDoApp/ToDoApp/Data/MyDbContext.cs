﻿using Microsoft.EntityFrameworkCore;

namespace ToDoApp.Data
{
    public class MyDbContext: DbContext
    {
        public MyDbContext(DbContextOptions options):base(options) { }

        #region
        public DbSet<Task> tasks { get; set; }
        #endregion

    }
}
