﻿using Microsoft.AspNetCore.Mvc;
using ToDoApp.Data;
using ToDoApp.Models;
using Task = ToDoApp.Data.Task;

namespace ToDoApp.Controllers
{

        public class TaskController : Controller
        {
            private readonly MyDbContext _context;

            public TaskController(MyDbContext context)
            {
                _context = context;
            }

            [HttpGet]
            public IActionResult Index()
            {
                var tasks = _context.tasks.ToList();
                return View(tasks);
            }

            [HttpGet]
            public IActionResult Create()
            {
                return View();
            }

            [HttpPost]
            public IActionResult Create(CreateTaskView task)
            {
                var tasks = new Task()
                {
                    Name= task.Name,
                    Description= task.Description,
                    deadline= task.deadline,
                    priority= task.priority,
                    status= task.status,
                };
                _context.tasks.Add(tasks);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }

            /*[HttpGet]
            public IActionResult View(int ID)
            {
                var task = _context.tasks.FirstOrDefault(x => x.ID == ID);
                if (task != null)
                {
                    var tasks = new ViewTask()
                    {
                        ID = task.ID,
                        NameTask = task.NameTask,
                        DescriptionTask = task.DescriptionTask,
                        Deadline = task.Deadline,
                        Priority = task.Priority,
                        StartupBase = task.StartupBase
                    };
                    return View("View", tasks);
                }
                return RedirectToAction("Index");
            }

            [HttpPost]
            public IActionResult View(ViewTask update)
            {
                var task = _context.tasks.Find(update.ID);
                if (task != null)
                {
                    task.NameTask = update.NameTask;
                    task.DescriptionTask = update.DescriptionTask;
                    task.NameTask = update.NameTask;
                    task.Deadline = update.Deadline;
                    task.Priority = update.Priority;
                    task.StartupBase = update.StartupBase;
                    _context.SaveChanges();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }

            [HttpPost]
            public IActionResult Delete(ViewTask update)
            {
                var task = _context.tasks.Find(update.ID);
                if (task != null)
                {
                    _context.tasks.Remove(task);
                    _context.SaveChanges();

                }
                return RedirectToAction("Index");
            }

            [HttpGet]
            public IActionResult Search(string name)
            {
                var task = _context.tasks.Find(name);
                if (task != null)
                {
                    return RedirectToAction("Index", task);
                }
                return RedirectToAction("Index");
            }
        }*/


    }
}
