﻿namespace ToDoApp.Services
{
    public interface ITaskRepository
    {
        List<Data.Task> GetAll();
        Data.Task GetById(int id);
        Data.Task Create(Data.Task task);
        void Update(Data.Task task);
        void Delete(int id);
    }
}
