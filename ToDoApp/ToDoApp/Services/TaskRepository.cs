﻿using static ToDoApp.Services.TaskRepository;
using ToDoApp.Data;
using Task = ToDoApp.Data.Task;

namespace ToDoApp.Services
{
    public class TaskRepository
    {
        public class TaksRepository : ITaskRepository
        {
            private readonly MyDbContext _context;

            public TaksRepository(MyDbContext context)
            {
                _context = context;
            }

            public Task Create(Task task)
            {
                var tasks = new Task()
                {
                    Name= task.Name,
                    Description= task.Description,
                    deadline=task.deadline,
                    priority=task.priority,
                    status=task.status,
                };
                _context.Add(tasks);
                _context.SaveChanges();
                return new Data.Task()
                {
                    Name = task.Name,
                    Description = task.Description,
                    deadline = task.deadline,
                    priority = task.priority,
                    status = task.status,
                };
            }

            public void Delete(int id)
            {
                var task = _context.tasks.SingleOrDefault(ta => ta.Id.Equals(id));
                if (task != null)
                    _context.Remove(task);
                _context.SaveChanges();
            }

            public List<Task> GetAll()
            {
                var tasks = _context.tasks.ToList();
                return tasks;
            }

            public Task GetById(int id)
            {
                var task = _context.tasks.SingleOrDefault(ta => ta.Id.Equals(id));
                if (task != null)
                    return task;
                return null;
            }

            public void Update(Task task)
            {
                var tasks = _context.tasks.SingleOrDefault(ta => ta.Id == task.Id);
                tasks.status = task.status;

                _context.SaveChanges();
            }
        }
    }
    }

